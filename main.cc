#include <iostream>
#include <cstdint>
#include <vector>
#include <fstream>
#include <string>
#include <math.h>

#include <xpu.h>
#include <xpu/runtime>

#include <core/circuit.h>
#include <qcode/quantum_code_loader.h>
#include <core/error_model.h>

int main(int argc, char **argv){
	//variables
	std::ifstream file;
	file.open("settings.txt");
	int a, a1, a2;
	double boundary = 0.001;
	int push, analyse_gates_i, measure_i, show_circuit_i, error_testing_i, parallel_optimize_i; 
	double error, chance; 
	char set;
	xpu::timer timer_make;
	xpu::timer timer_execute;
	std::string l1, l2, l3, l4, l5, l6, l7;
	std::string i1, i2, i3, i4, i5, i6, i7;
	//input settings
	while(set!='y' && set!='n'){
		std::cout << "change settings: [y/n] ";
		std::cin >> set;
	}
	if(set == 'y'){
		std::getline(file,l1); //push
		std::getline(std::cin,i1);
		if(l1.empty()){push =0;}
		else{push = std::stoi(l1);}
		do{std::cout << "push is now: " << push << " insert new value: ";
			std::getline(std::cin,i1);
			if(!i1.empty()){push = std::stoi(i1);}
		}while( push < 1);
		std::getline(file,l2); //measurement
		if(l2.empty()){measure_i =0;}
		else{measure_i = std::stoi(l2);}
		do{std::cout << "measure is now: " << measure_i << " insert new value: ";
			std::getline(std::cin,i2);
			if(!i2.empty()){measure_i = std::stoi(i2);}
		}while(measure_i!=1 && measure_i!=0);
		std::getline(file,l3); //show_circuit
		if(l3.empty()){show_circuit_i =0;}
		else{show_circuit_i = std::stoi(l3);}
		do{std::cout << "show_circuit is now: " << show_circuit_i << " insert new value: ";
			std::getline(std::cin,i3);
			if(!i3.empty()){show_circuit_i = std::stoi(i3);}
		}while(show_circuit_i!=1 && show_circuit_i!=0);
		std::getline(file,l4); //error_testing
		if(l4.empty()){error_testing_i =0;}
		else{error_testing_i = std::stoi(l4);}
		do{std::cout << "error_testing is now: " << error_testing_i << " insert new value: ";
			std::getline(std::cin,i4);
			if(!i4.empty()){error_testing_i = std::stoi(i4);}
		}while(error_testing_i!=1 && error_testing_i!=0);
		std::getline(file,l5); //parallel_optimize
		if(l5.empty()){parallel_optimize_i =0;}
		else{parallel_optimize_i = std::stoi(l5);}
		do{std::cout << "parallel_optimize is now: " << parallel_optimize_i << " insert new value: ";
			std::getline(std::cin,i5);
			if(!i5.empty()){parallel_optimize_i = std::stoi(i5);}
		}while(parallel_optimize_i!=1 && parallel_optimize_i!=0);
		std::getline(file,l6); //analyse_gates
		if(l6.empty()){analyse_gates_i =0;}
		else{analyse_gates_i = std::stod(l6);}
		do{std::cout << "analyse_gates is now: " << analyse_gates_i << " insert new value: ";
			std::getline(std::cin,i6);
			if(!i6.empty()){analyse_gates_i = std::stod(i6);}
		}while(analyse_gates_i!=0 && analyse_gates_i!=1);
		std::getline(file,l7); //error
		if(l7.empty()){error =0;}
		else{error = std::stod(l7);}
		do{std::cout << "error is now: " << error << " insert new value: ";
			std::getline(std::cin,i7);
			if(!i7.empty()){error = std::stod(i7);}
		}while(error<0 || error>1);
		std::cout << "--------------------------------------" << std::endl;
		file.close();
		std::ofstream file;
		file.open("settings.txt");
		file << push << '\n';
		file << measure_i << '\n';
		file << show_circuit_i << '\n';
		file << error_testing_i << '\n';
		file << parallel_optimize_i << '\n';
		file << calculate_push_i << '\n';
		file << error << '\n';
		file << analyse_gates_i << '\n';
		file.close();
	}
	else{
		std::getline(file,l1); push = std::stoi(l1);
		std::getline(file,l2); measure_i = std::stoi(l2);
		std::getline(file,l3); show_circuit_i = std::stoi(l3);
		std::getline(file,l4); error_testing_i = std::stoi(l4);
		std::getline(file,l5); parallel_optimize_i = std::stoi(l5);
		std::getline(file,l6); calculate_push_i = std::stoi(l6);
		std::getline(file,l7); error = std::stod(l7);
		std::getline(file,l6); analyse_gates_i = std::stoi(l6);
		file.close();
	}
	//input error testing
	if(error_testing_i==1){
		do{std::cout << "how many bits for one int: ";
			std::cin >> a;}while(a<1);
		a1 = pow(2,a)-1;
		a2 = a1;
	}
	//input numbers
	else{
		do{std::cout << "int 1: ";
			std::cin >> a1;}while(a1<0);
		do{std::cout << "int 2: ";
			std::cin >> a2;}while(a2<0);
	}
	//general
	int ans = a1+a2;
	int maxint = (a1>a2) ? a1 : a2;
	int size =0; //amount of qubits for one number: N
	while (maxint >= pow(2,size)){size++;}
	//input to binary
	int q1[size], q2[size]; // q1=x0 x1 x2 .. a1=2⁶x0+2⁵x1+2⁴x2..
	for(int j=size-1;a1>0;j--){q1[j]=a1%2; a1=(a1-(a1%2))/2;}
	for(int j=size-1;a2>0;j--){q2[j]=a2%2; a2=(a2-(a2%2))/2;}
	//building circuit
		uint32_t n=2*size+1; //totaal amount of qubits
		qx::circuit c(n);
		timer_make.start();
	//adding gates
		//input
		for(int j=0;j<size;j++){if(q1[j]==1){c.add(new qx::pauli_x(j+1));}}
		for(int j=0;j<size;j++){if(q2[j]==1){c.add(new qx::pauli_x(j+size+1));}}
		if(analyse_gates_i==1){
			std::cout << "input: " << c.size() << std::endl;
		}
		//qft
		for(int j=0;j<=size;j++){
			c.add(new qx::hadamard(j));
			for(int k=j;k<size;k++){c.add(new qx::ctrl_phase_shift(k+1,j));}
		}
		if(analyse_gates_i==1){
			std::cout << "qft: " << c.size() << std::endl;
		}
		//addition
		for(int j=0;j<size;j++){
			c.add(new qx::swap(j, j+size));
			for(int k=j;k<size;k++){c.add(new qx::ctrl_phase_shift(k+size+1,j+size));}
			c.add(new qx::swap(j, j+size));
			c.add(new qx::cphase(j+size+1,j+1));
		}
		if(analyse_gates_i==1){
			std::cout << "addition: " << c.size() << std::endl;
		}
		//qft invers
		for(int j=size;j>=0;j--){
			for(int k=size;k>j;k--){
				double phi = -1 * M_PI / (pow(2,k-j));
				c.add(new qx::cnot(k, j));//c-not
				c.add(new qx::rz(j,-1*phi/2));//Rz(-phi/2) op targ
				c.add(new qx::cnot(k, j));//c-not
				c.add(new qx::rz(j, phi/2));//Rz(phi/2) op targ
			}
			c.add(new qx::hadamard(j));
		}
		if(analyse_gates_i==1){
			std::cout << "qft⁻¹: " << c.size() << std::endl;
		}
	//parallelization
	if(parallel_optimize_i==1){
		std::vector< qx::gate * > para_verz ={}; //gates to add
		std::vector< std::vector<int> > pos2 = {}; //position gates
		std::vector< std::vector<int> > q;//circuit in queue's
		std::vector<int> position = {};//the other values
		std::vector<int> dept (c.size(),0);
		int loop =n; int min;
		for(int j = 0; j<n; j++){q.push_back(std::vector<int>());}
		for(int gate_it =0; gate_it < c.size(); gate_it ++){
			qx::gate * gate = c.get(gate_it);//filling q
			std::vector<uint32_t> control = gate -> control_qubits();
			std::vector<uint32_t> target = gate -> target_qubits();
			for(int j =0; j<control.size(); j++){
				q[control[j]].push_back(gate_it);dept[gate_it]++;
			}
			for(int j =0; j<target.size(); j++){
				q[target[j]].push_back(gate_it);dept[gate_it]++;
			}
			if(!target.empty()){loop = target[0];}
		}
		while(loop<n){
			min = q[loop][0];
			position.clear();
			for(int j = 0; j < n; j++){
				if(!q[j].empty() && q[j][0]<min){min = q[j][0];}
			}//searching lowest value
			for(int j = 0; j < n; j++){
				if(!q[j].empty() && q[j][0]!=min){position.push_back(q[j][0]);}
			}//searching all other values
			std::vector<int> flaw = {};
			for(int i =0; i<position.size();i++){//wrong gates
				int count = 0;
				for(int j = 0; j<position.size();j++){
					if(position[j]==position[i]){count++;}
				}
				if(count < dept[position[i]]){
					flaw.push_back(position[i]);
				}
			}
			for(int i =0; i<flaw.size();i++){//del gates
				for(int j =position.size()-1; j>=0;j--){
					if(flaw[i]==position[j]){
						position.erase(position.begin()+j);
					}
				}
			}
			for(int j = 0; j<position.size();j++){//unique
					for(int k = j+1; k<position.size();k++){
					if(position[j]==position[k]){
						position.erase(position.begin()+k);
						k--;
					}
				}
			}
			if(position.empty()){
				for(int j=0;j<n;j++){
					if(!q[j].empty() && q[j][0]==min){
						q[j].erase(q[j].begin());
					}
				}
			}//del lowest value
			else{//building parallel gate& adding gates
				pos2.push_back(position);
				int g = pos2.size()-1;
				pos2[g].insert(pos2[g].begin(), min);
				qx::parallel_gates * parallel = new qx::parallel_gates();
				parallel -> add(c.get(min));
				for(int k =0 ;k<n;k++){
					if(!q[k].empty() && q[k][0]==min){
						q[k].erase(q[k].begin());
					}
				}
				for(int j =0; j<position.size(); j++){
					parallel -> add(c.get(position[j]));
					for(int k =0 ;k<n;k++){
						if(!q[k].empty() && q[k][0]==position[j]){
							q[k].erase(q[k].begin());
						}
					}
				}
				para_verz.push_back(parallel);
			}
			loop =0;//checking if done
			while(q[loop].empty()){loop++;}
		}
		//insert parallel gate& del original gate
		for(int j =0; j<para_verz.size(); j++){
			c.erase(pos2[j][0]);
			c.insert(pos2[j][0],para_verz[j]);
			pos2[j].erase(pos2[j].begin());
		}//del other gates
		std::vector<int> pos3 = {};
		for(int j =0; j<pos2.size(); j++){
			for(int k = 0; k<pos2[j].size(); k++){
				if(pos3.empty()){pos3.push_back(pos2[0][0]);}
				else{ 
					int it =0;//biggest value at the back
					while(pos2[j][k]>pos3[it] && it<pos3.size()){it++;}
					pos3.insert(pos3.begin()+it,pos2[j][k]);
				}
			}
		}
		while(!pos3.empty()){
			c.erase(pos3.back());
			pos3.pop_back();
		}
	}
	//measurement
	if(measure_i==1){c.add(new qx::measure());}
	timer_make.stop();
	timer_execute.start();
	//gates output
	if(show_circuit_i==1){c.dump();}
	if(analyse_gates_i==1){
		std::cout << "parallel: " << c.size() << std::endl;
		return 0;
	}
	//execute
	qx::qu_register reg(n);
	xpu::complex_d null;
	std::vector<int> state;
	std::vector<int> values;
	std::vector<double> prob;
	if(error==0){//perfect circuit
		c.execute(reg);
		for(int i=0;i<pow(2,n);i++){
			if(reg[i]!=null){
				double chance = pow(reg[i].re,2)+powf(reg[i].im,2); 
				if(chance>boundary){ 
					state.push_back(i);
					prob.push_back(chance);
				}
			}
		}
	}
	else{//error circuit
		qx::depolarizing_channel dep_ch(&c, n, error);
		for (int j=0; j<push;j++){
			reg.reset();
			qx::circuit * noisy_c = dep_ch.inject(false);
			noisy_c -> execute(reg);
			for (int i=0; i<pow(2,n); i++){
				if(reg[i]!=null){
					chance = pow(reg[i].re,2)+powf(reg[i].im,2);
					if (chance > boundary){
						if(state.empty()){//new state
							state.push_back(i);
							prob.push_back(chance/push);
						}
						else{//adding probability
							int k = 0;
							while (state[k] != i && k<state.size()){k++;}
							if (state[k] == i){prob[k] += chance/push;}
							else{
								state.push_back(i);
								prob.push_back(chance/push);
							}
						}
					}
				}
			}
		}
	}
	timer_execute.stop();
	//states to values
	for(int k =0; k<state.size(); k++){
		int m = state[k];
		for (int nn = n; nn>size; nn--){//del int b from front
			if(m>pow(2, nn)){m-=pow(2,nn);}
		}
		int mm =0;//the value
		for(int k =0;k<=size;k++){//reverse bit order
			if(m>=pow(2,size-k)){m-=pow(2,size-k);mm+=pow(2,k);}
		}
		values.push_back(mm);
	}
	//adding same values
	for(int k = 0; k+1 < values.size(); k++){
		for(int j = k+1; j<values.size();j++){
			if(values[k]==values[j]){
				prob[k]+=prob[j];
				values.erase(values.begin()+j);
				prob.erase(prob.begin()+j);
				j--;
			}
		}
	}
	//output
	std::cout << "[size: " << size << "  qubits: " << n << "  error: " << error << "  gates: " << c.size() << "  push: " << push << "  measure: " << measure_i << "  parallel: " << parallel_optimize_i << "]" << std::endl;
	std::cout << "final probability: " << std::endl;
	double total =0.0;
	double ans_prob;
	double best_prob =0.0;
	int best =0;
	std::string push2_string;
	for(int i = values.size()-1;i>=0;i--){
		if(prob[i] > boundary/10){
			std::cout << "Value:  " << values[i] <<  " with prob: " << prob[i] << std::endl; 
			total += prob[i];
		}
		if(values[i] == ans){ans_prob = prob[i];}
		if(prob[i]>best_prob && values[i]!=ans){
			best = values[i]; 
			best_prob = prob[i];
		}
	}
	std::cout << "                   ------------- +" << std::endl;
	std::cout << "                    " << total << std::endl;
	std::cout << "answ: " << ans << ", prob: " << ans_prob << std::endl;
	std::cout << "best: " << best << ", prob: " << best_prob << std::endl;
	std::cout << "time to build: " << timer_make.elapsed()<< ", time to run: " << timer_execute.elapsed() << std::endl;
	return 0;
}
/*
changes made in qx:
gate.h -> Ln 2287 & 2288 (+1 to cr gate)
curcuit.h -> Ln 132 & 155 (//println)
curcuit.h -> Ln 176 - 179 (erase functie)

TU Delft, The Netherlands
author: Menno Looman
date: 2018-07-02
*/